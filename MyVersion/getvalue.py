from MyVersion.error import error
from MyVersion.perturb import moveRandomPoints
from MyVersion.checkrange import isInFilterRange
from MyVersion.adjustdf import adjustdf
from MyVersion.utils import SimulatedAnnealing
from MyVersion.getscagnostics import getscagnostics
from MyVersion.savedata import savethedata
from tqdm import tqdm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


from numpy.random import randn
from random import randrange, uniform


def getdf(numpoints_min,numpoints_max,irand): #A first and simple first DF/Scatterplot on which we perturb. This needs to be adjusted.
    #This function is not used anymore because dont want a random starting position each time. So old df is loaded with olddata()


    #irand = randrange(numpoints_min, numpoints_max)
    x1 = np.random.randint(1, 100, irand)
    y1 = np.random.randint(1, 100, irand)

    #print(irand)
    adf = pd.DataFrame({'x': x1, 'y': y1})
    #random_integer_array = random.randint(10, 30, size=(100, 1))
    #print(random_integer_array)
    #OriginalMeasures = getscagnostics(x1, y1, Scagnoticversion)
    #print(OriginalMeasures)
    #plt.scatter(x1, y1)
    #plt.show()
    np.save("x1.npy", x1)
    np.save("y1.npy", y1)
    return x1, y1
    #print(x1)
#   getdf()

#getdf()
def olddata(): #As mentioned before, an old df/scatterplot is loaded on which will be permutated.
    #OriginalArray_X = np.load('random_integer_array.npy')
    #OriginalArray_Y = np.load('random_integer_array1.npy')
    OriginalArray_X = np.load('x1.npy')
    OriginalArray_Y = np.load('y1.npy')

    return OriginalArray_X, OriginalArray_Y

def get_value(configurations, direction,memory,processnumber,metrics): #The main function

    #set the config
    permutationsize=configurations["permutationsize"]

    optimizer=configurations["Optimizer"]
    maxIterations=configurations["maxIterations"]
    savedata=configurations["savedata"]
    #metrics=configurations["metrics"]

    showstatus=configurations["showstatus"]
    old_data= configurations["old_data"]
    Scagnoticversion = configurations["Scagnoticversion"]
    numpoints_min=configurations["Number_of_points_in_starting_position_min"]
    numpoints_max=configurations["Number_of_points_in_starting_position_max"]
    showplots=configurations["showplots"]
    rangemargin=configurations["Strictness_of_points"]
    numberofclasses = configurations["Number_of_classes_per_metric"]
    sizeofclass = 1 / numberofclasses
    newclass = sizeofclass
    rangemargin = newclass / 2
    silent=configurations["silent"]
    savelocation=configurations["savelocation"]
    countprevious=0
    irand = randrange(numpoints_min, numpoints_max)

    if old_data == "previous":
        if memory>=2:
            for key, value in direction.items():
                if value !=0:
                #print('we load old')
                    print("old data")
                    OriginalArray_X = np.load('CurrentArray_X.npy')
                    OriginalArray_Y = np.load('CurrentArray_Y.npy')
                else:
                    OriginalArray_X, OriginalArray_Y= getdf(numpoints_min, numpoints_max,irand)
                    #print(OriginalArray_X)
                    print("new data")
        else:
            OriginalArray_X, OriginalArray_Y = getdf(numpoints_min, numpoints_max,irand)

    if old_data == "old":
        OriginalArray_X, OriginalArray_Y = olddata()     #Load data
        if showplots=="True":
            plt.scatter(OriginalArray_X, OriginalArray_Y)
            plt.show()
    if old_data == "new":
        OriginalArray_X, OriginalArray_Y = getdf(numpoints_min,numpoints_max,irand)
        if showplots == "True":

            plt.scatter(OriginalArray_X, OriginalArray_Y)
            plt.suptitle(f'Title before plotting, with config {direction} 1', fontsize=5)
            #plt.xlabel(f'{CurrentMeasures}', fontsize=5)
            plt.show()

    #Original Positions
    OriginalMeasures = getscagnostics(OriginalArray_X, OriginalArray_Y, Scagnoticversion)
    OriginalError = error(qmresult=OriginalMeasures, direction=direction)
    #print(f"Starting postion:{OriginalMeasures}")
    CurrentArray_X=OriginalArray_X #setting the starting point to the current df/scatterplot
    CurrentArray_Y=OriginalArray_Y

    #plt.scatter(OriginalArray_X, OriginalArray_Y) #Original Scatterplot
    #plt.show()
    #plt.clf()

    time=0 #To see how many iterations have been used
    inRange = False



    if silent != 'True':
        print(f"We are currentlt training for: {direction}")     #Provide a overview

    while not inRange:
        #print(f"before measureing {CurrentArray_X}")
        #print(f"before measureing {CurrentArray_Y}")

        CurrentMeasures = getscagnostics(CurrentArray_X, CurrentArray_Y, Scagnoticversion)  # Calculate current measures
        #print(CurrentMeasures)
        #Calculate current measures
        CurrentError=error(qmresult=CurrentMeasures, direction=direction) #Calculate current error. Error= How far the scagnostics are from their desired fit (So optimal is 0.)
        print(CurrentMeasures)
        print(f"current error at start {CurrentError}")
        simAnnealing = SimulatedAnnealing(maxIterations, CurrentError) #Used to accept worse solution if stuck in local minimum.

        for i in tqdm(range(maxIterations)):
            #for i in tqdm(range(maxIterations)):
            memory = memory + 1
            time = time + 1


            #Optimizers
            if optimizer == 'test':
                Adjusted_X, Adjusted_Y = moveRandomPoints2(permutationsize, domainMin=0, domainMax=100, OriginalArray_Y=CurrentArray_Y,
                                                          OriginalArray_X=CurrentArray_X)  # Function that moves the points into a random direction
                AdjustedMeasures = getscagnostics(Adjusted_X, Adjusted_Y, Scagnoticversion)
                # AdjustedMeasures= measures  # calculate how the measures have changed
                AdjustedError = error(qmresult=AdjustedMeasures,
                                      direction=direction)  # calculate how the error is changed
                # print(AdjustedError)
                if CurrentError < AdjustedError:  # If the error is reduced, we accept the knew df/scatterplot
                    if showstatus == "True":  # Optimal function to see how the df is adjusted over time
                        print(f" Better. CurrentError: {CurrentError} and the AdjustedError {AdjustedError}")
                    CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError = adjustdf(Adjusted_X, Adjusted_Y,
                                                                                             AdjustedMeasures,
                                                                                             AdjustedError)  # the current df is adjusted to the permutated one

                #if simAnnealing.shouldAccept(
                #        AdjustedError) and CurrentError > AdjustedError:  # if the simAnnealing accept the new solution
                #    if showstatus == "True":
                #        print(f" Heated. CurrentError: {CurrentError} and the AdjustedError {AdjustedError}")
                #    CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError = adjustdf(Adjusted_X, Adjusted_Y,AdjustedMeasures,AdjustedError)

                # del AdjustedMeasures

                simAnnealing.updateTemp()  # the temp is update each iteration, if updated or not

            if optimizer=='random':


                #Adjusted_X, Adjusted_Y = moveRandomPoints(domainMin=0, domainMax=1000, OriginalArray_Y=CurrentArray_Y,
                #                                          OriginalArray_X=CurrentArray_X)  # Function that moves the points into a random direction
                irand = randrange(numpoints_min, numpoints_max)
                Adjusted_X = np.random.randint(1, 100, irand)
                Adjusted_Y = np.random.randint(1, 100, irand)
                AdjustedMeasures = getscagnostics(Adjusted_X, Adjusted_Y, Scagnoticversion)

                #print(AdjustedMeasures)
                #AdjustedMeasures= measures  # calculate how the measures have changed
                AdjustedError = error(qmresult=AdjustedMeasures,
                                      direction=direction)  # calculate how the error is changed
                #print(AdjustedError)
                if CurrentError < AdjustedError:  # If the error is reduced, we accept the knew df/scatterplot
                    if showstatus == "True":  # Optimal function to see how the df is adjusted over time
                        print(f" Better. CurrentError: {CurrentError} and the AdjustedError {AdjustedError}")
                    CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError = adjustdf(Adjusted_X, Adjusted_Y,
                                                                                             AdjustedMeasures,
                                                                                            AdjustedError)  # the current df is adjusted to the permutated one

                if simAnnealing.shouldAccept(AdjustedError) and CurrentError > AdjustedError: #if the simAnnealing accept the new solution
                    if showstatus == "True":
                        print(f" Heated. CurrentError: {CurrentError} and the AdjustedError {AdjustedError}")
                    CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError= adjustdf(Adjusted_X, Adjusted_Y, AdjustedMeasures, AdjustedError)


                #del AdjustedMeasures

                simAnnealing.updateTemp()

            if optimizer=='annealing':


                Adjusted_X, Adjusted_Y = moveRandomPoints(permutationsize,domainMin=0, domainMax=100, OriginalArray_Y=CurrentArray_Y,
                                                          OriginalArray_X=CurrentArray_X)  # Function that moves the points into a random direction
                AdjustedMeasures = getscagnostics(Adjusted_X, Adjusted_Y,Scagnoticversion)
                #print(AdjustedMeasures)
                #AdjustedMeasures= measures  # calculate how the measures have changed
                AdjustedError = error(qmresult=AdjustedMeasures,
                                      direction=direction)  # calculate how the error is changed
                #print(AdjustedError)
                if CurrentError < AdjustedError:  # If the error is reduced, we accept the knew df/scatterplot
                    if showstatus == "True":  # Optimal function to see how the df is adjusted over time
                        print(f" Better. CurrentError: {CurrentError} and the AdjustedError {AdjustedError}")
                    CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError = adjustdf(Adjusted_X, Adjusted_Y,
                                                                                             AdjustedMeasures,
                                                                                             AdjustedError)  # the current df is adjusted to the permutated one

                if simAnnealing.shouldAccept(AdjustedError) and CurrentError > AdjustedError: #if the simAnnealing accept the new solution
                    if showstatus == "True":
                        print(f" Heated. CurrentError: {CurrentError} and the AdjustedError {AdjustedError}")
                    CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError= adjustdf(Adjusted_X, Adjusted_Y, AdjustedMeasures, AdjustedError)

                #del AdjustedMeasures
                if old_data =="previous" and CurrentError>0.4:
                    print("we went too far")
                    CurrentArray_X, CurrentArray_Y = getdf(numpoints_min, numpoints_max,irand)


                simAnnealing.updateTemp() #the temp is update each iteration, if updated or not

            #filter that checks if the current results are in the range
            inrage = isInFilterRange(silent=silent ,qmresult=CurrentMeasures, direction=direction, maxiterations=maxIterations,
                                     time=time, rangemargin=rangemargin)
            if inrage == True:
                print(CurrentMeasures)
                #direction
                #dictionary_copy = direction.copy()
                #succesdirections.append(dictionary_copy)
                if old_data == "previous":
                    #print("we save")
                    #print(AdjustedMeasures)
                    np.save("CurrentArray_X.npy", CurrentArray_X)
                    np.save("CurrentArray_Y.npy", CurrentArray_Y)
                    #print(CurrentArray_X)
                #df = pd.DataFrame({'x': [CurrentArray_X], 'y': [CurrentArray_Y]})
                if savedata == "True": #to reduce overload

                    savethedata(direction=direction, measures=CurrentMeasures, CurrentArray_X=CurrentArray_X,
                             CurrentArray_Y=CurrentArray_Y, savelocation=savelocation, processnumber=processnumber,metrics=metrics,irand=irand)
                    if showplots == "True":
                        plt.scatter(CurrentArray_X, CurrentArray_Y)
                        for key in CurrentMeasures:
                            # rounding to K using round()
                            CurrentMeasures[key] = round(CurrentMeasures[key], 3)
                        plt.suptitle(f'fitting succes, with config {direction} 1',fontsize=5)
                        plt.xlabel(f'{CurrentMeasures}', fontsize=5)
                        plt.show()
                #avg_itertaitions.append(time)
                inRange = True
                x = ' '  # create empty line in console
                print(x)
                status = "succes"
                return  time, status, direction, memory
                break




        if time == maxIterations: #if we do not get Inrange within the max iterations
            #dictionary_copy = direction.copy()
            #faileddirections.append(dictionary_copy)
            if showplots == "True":
                plt.scatter(CurrentArray_X, CurrentArray_Y)
                plt.suptitle(f'fitting failed, with config {direction} 1', fontsize=5)
                for key in CurrentMeasures:
                    # rounding to K using round()
                    CurrentMeasures[key] = round(CurrentMeasures[key], 3)
                plt.xlabel(f'{CurrentMeasures}', fontsize=5)
                plt.show()

            if silent != "True":
                print(f"Fitting failed: The ending position was was: {CurrentMeasures}.") #show the status

            x = ' '  # create empty line in console
            print(x)
            inRange = True
            status="fail"
            return time, status, direction, memory
    return time, status, direction, memory


