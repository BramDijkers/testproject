import random
import math


class SimulatedAnnealing(object):

    def __init__(self, iterations, startError, p1=0.7, pEnd=0.001):
        # Probability of accepting worse solution at the start
        self.p1 = p1
        # Probability of accepting worse solution at the end
        self.pEnd = pEnd

        # Initial temperature
        self.t1 = -1.0 / math.log(self.p1)
        # Final temperature
        self.tEnd = -1.0 / math.log(self.pEnd)
        # Fractional reduction every cycle
        self.frac = (self.tEnd / self.t1)**(1.0 / (iterations - 1.0))

        # DeltaE Average
        self.DeltaE_avg = startError

        # The current temperature
        self.t = self.t1

        self.na = 0.0

        # # generate probability of acceptance
        # p = math.exp(-DeltaE / (DeltaE_avg * t))
        # # determine whether to accept worse point
        # if (random.random() < p):
        #     # accept the worse solution
        #     accept = True
        # else:
        #     # don't accept the worse solution
        #     accept = False

    def updateTemp(self):
        self.t = self.frac * self.t

    def _updateAccepted(self, deltaE):
        self.na = self.na + 1.0
        # update DeltaE_avg
        self.DeltaE_avg = (self.DeltaE_avg * (self.na - 1.0) + deltaE) / self.na

    def shouldAccept(self, deltaE):
        # generate probability of acceptance
        if deltaE!=0:
            p = math.exp(-deltaE / (self.DeltaE_avg * self.t))
        else:
            p=0# determine whether to accept worse point
        if (random.random() < p):
            # accept the worse solution
            self._updateAccepted(deltaE)
            return True
        else:
            # don't accept the worse solution
            return False
