import re
import pandas as pd
import os.path
from pathlib import Path
import os.path
from os import path
import matplotlib.pyplot as plt
column_names = ['id', 'clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
                    "Stringy", "Monotonic", 'image_location']
maindf = pd.DataFrame(columns=column_names)

def savethedata(direction, measures, CurrentArray_X, CurrentArray_Y,savelocation,processnumber,metrics, irand):
    column_names = ['id', 'clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
                    "Stringy", "Monotonic", 'image_location']

    column_names = ['id', 'Clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
                    "Stringy", "Monotonic", 'irand', 'image_location']
    maindf = pd.DataFrame(columns=column_names)
    print(f"we save df at: {metrics}")
    dictstring = str(direction)  # convert the current fit to a string so we know what kind of df it is
    string_no_punctuation = re.sub("[^\w\s]", "", dictstring)  # deletes punctuation
    string_no_punctuation= string_no_punctuation.replace('Clumpy ',"Cl")
    string_no_punctuation = string_no_punctuation.replace('Outlying ', "Ou")
    string_no_punctuation = string_no_punctuation.replace('Skewed ', "Ske")
    string_no_punctuation = string_no_punctuation.replace('Sparse ', "Sp")
    string_no_punctuation = string_no_punctuation.replace('Striated ', "St")
    string_no_punctuation = string_no_punctuation.replace('Convex ', "Co")
    string_no_punctuation = string_no_punctuation.replace('Skinny ', "Ski")
    string_no_punctuation = string_no_punctuation.replace('Stringy ', "St")
    string_no_punctuation = string_no_punctuation.replace('Monotonic ', "Mo")
    my_file = Path(f"permutation/data/{savelocation}/df/{metrics}.pkl")

    try:
        my_abs_path = my_file.resolve(strict=True)
    except FileNotFoundError:
        maindf.to_pickle(f"permutation/data/{savelocation}/df/{metrics}.pkl")  # save the df

    my_file = Path(f"permutation/data/{savelocation}/scatterplots/{metrics}")

    testpath = path.isdir(f"permutation/data/{savelocation}/scatterplots/{metrics}")
    if testpath==False:
        os.mkdir(f"permutation/data/{savelocation}/scatterplots/{metrics}")


    Clumpy = measures.get("Clumpy")
    Outlying = measures.get("Outlying")
    Skewed = measures.get("Skewed")
    Sparse = measures.get("Sparse")
    Striated = measures.get("Striated")
    Convex = measures.get("Convex")
    Skinny = measures.get("Skinny")
    Stringy = measures.get("Stringy")
    Monotonic = measures.get("Monotonic")

    plt.scatter(CurrentArray_X, CurrentArray_Y)
    plt.axis('off')
    images_dir = f"permutation/data/{savelocation}/scatterplots/{metrics}"
    image_name = f"{images_dir}/{string_no_punctuation}-{processnumber}.png"
    plt.savefig(f"{images_dir}/{string_no_punctuation}-{processnumber}.png")
    plt.clf()
    maindf = pd.read_pickle(f"permutation/data/{savelocation}/df/{metrics}.pkl")

    test = maindf.empty
    #print(test)
    series_obj = pd.Series(
        [string_no_punctuation, Clumpy, Outlying, Skewed, Sparse, Striated, Convex, Skinny, Stringy, Monotonic, irand, image_name],
        index=maindf.columns)
    maindf = maindf.append(series_obj, ignore_index=True)
    #print(maindf.describe(include='all'))
    maindf.to_pickle(f"permutation/data/{savelocation}/df/{metrics}.pkl")


    #return maindf


