def filter(direction):
    breakloop=False

    #Known for sure based on function:
    if "Convex" in direction and "Skinny" in direction :
        if direction["Convex"] >= 0.8 and direction["Skinny"] >= 0.8 or direction["Convex"] <= 0.2 and direction["Skinny"] <= 0.2:
            breakloop = True


    #own context HH:
    if "Outlying" in direction and "Clumpy" in direction :
        if direction["Outlying"] >= 0.8 and direction["Clumpy"] >= 0.8:
            breakloop=True
    if "Outlying" in direction and "Striated" in direction :
        if direction["Outlying"] >= 0.8 and direction["Striated"] >= 0.8:
            breakloop = True
    if "Outlying" in direction and "Convex" in direction:
        if direction["Outlying"] >= 0.8 and direction["Convex"] >= 0.8:
            breakloop=True
    if "Outlying" in direction and "Skinny" in direction:
        if direction["Outlying"] >= 0.8 and direction["Skinny"] >= 0.8:
            breakloop = True
    if "Outlying" in direction and "Stringy" in direction:
        if direction["Outlying"] >= 0.8 and direction["Stringy"] >= 0.8:
            breakloop=True
    if "Outlying" in direction and "Monotonic" in direction:
        if direction["Outlying"] >= 0.8 and direction["Monotonic"] >= 0.8:
            breakloop = True
    if "Convex" in direction and "Stringy" in direction:
        if direction["Convex"] >= 0.8 and direction["Stringy"] >= 0.8:
            breakloop=True
    if "Convex" in direction and "Monotonic" in direction:
        if direction["Convex"] >= 0.8 and direction["Monotonic"] >= 0.8:
            breakloop = True

    # own context HH:
    if "Skinny" in direction and "Stringy" in direction:
        if direction["Skinny"] >= 0.8 and direction["Stringy"] <= 0.2:
            breakloop = True
    if "Stringy" in direction and "Skinny" in direction:
        if direction["Stringy"] >= 0.8 and direction["Skinny"] <= 0.2:
         breakloop = True
    if "Striated" in direction and "Skinny" in direction:
        if direction["Striated"] >= 0.8 and direction["Skinny"] <= 0.2:
            breakloop = True



    #ownly use usfull info
    #if direction["Sparse"] == 0.4 or direction["Convex"] == 0.4 or direction["Skewed"] == 0.4:
    #    if direction["Sparse"] == 0.8 or direction["Convex"]  == 0.8 or direction["Skewed"] == 0.8:

    #if direction["Convex"] == 0.4:
    #    breakloop = True
    #if direction["Convex"]  == 0.8:
    #    breakloop = True

    #if direction["Skewed"]<= 0.2:
    #    breakloop=True




    return breakloop