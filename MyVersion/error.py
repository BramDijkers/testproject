

def error(qmresult, direction):
    errorValue = 0.0  # being maximally off for nine scagnostics values == 9.0

    for filter in direction:
        for qmName, qmRange in qmresult.items():

                if qmName == "Clumpy":
                    if "Clumpy" in direction:
                        target = direction["Clumpy"]
                        errorValue -= abs(qmRange - target)
                if qmName == "Monotonic":
                    if "Monotonic" in direction:
                        target = direction["Monotonic"]
                        errorValue -= abs(qmRange - target)
                if qmName == "Outlying":
                    if "Outlying" in direction:
                        target = direction["Outlying"]
                        errorValue -= abs(qmRange - target)
                if qmName == "Skewed":
                    if "Skewed" in direction:
                        target = direction["Skewed"]
                        errorValue -= abs(qmRange - target)
                if qmName == "Sparse":
                    if "Sparse" in direction:
                        target = direction["Sparse"]
                        errorValue -= abs(qmRange - target)
                if qmName == "Striated":
                    if "Striated" in direction:
                        target = direction["Striated"]
                        errorValue -= abs(qmRange - target)
                if qmName == "Convex":
                    if "Convex" in direction:
                        target = direction["Convex"]
                        errorValue -= abs(qmRange - target)
                if qmName == "Skinny":
                    if "Skinny" in direction:
                        target = direction["Skinny"]
                        errorValue -= abs(qmRange - target)
                if qmName == "Stringy":
                    if "Stringy" in direction:
                        target = direction["Stringy"]
                        errorValue -= abs(qmRange - target)
        return errorValue

