import json

default_configurations = {  # In case the configurations file is missing or incomplete
    "Optimizer": "annealing",
    "old_data": "yes",
    "Number_of_points_in_starting_position": 100,
    "Strictness_of_points": 0.165,
    "Number_of_classes_per_metric": 3,
    "maxIterations": 1000,
    "savedata": "True",
    "showstatus": "Yes",
    "showplots" : "No"
}

def load_configurations(config_file_path) -> dict:
    """Load the configurations from file and use default configurations for missing keys"""
    file = open(config_file_path)
    configurations = json.load(file)

    missing_configurations = {k: v for k, v in default_configurations.items() if k not in configurations}
    #if
    for k, v in missing_configurations.items():
        print(f"Missing '{k}' configuration preference. Using default value '{v}'")
        configurations[k] = v

    #if configurations["Doall"]=="False" and configurations["repeatprocess"]>1:
    #    print("ERROR: Doall cant be false with multiple repeatprocess")
    #    sys.exit(1)
    return configurations
