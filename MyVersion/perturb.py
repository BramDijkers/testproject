from random import random, randint


def moveRandomPoints(permutationsize, domainMax, domainMin, OriginalArray_X, OriginalArray_Y):
    rangeDomain = domainMax - domainMin
    maxStep = rangeDomain * permutationsize #how large the change can be
    minStep = -maxStep
    Adjusted_X= []
    Adjusted_Y= []
    count=0
    lenght = len(OriginalArray_X)
    for i in OriginalArray_X:
        rand_x = randint(minStep, maxStep)
        rand_y = randint(minStep, maxStep)
        if i + rand_x < domainMax and i + rand_x > domainMin:
            i = i + rand_x
            Adjusted_X.append(i)
        else:
            Adjusted_X.append(i)
    for i in OriginalArray_Y:
        rand_x = randint(minStep, maxStep)
        rand_y = randint(minStep, maxStep)
        #print(i)
        if i + rand_y < domainMax and i + rand_y > domainMin:
            i = i + rand_y
            Adjusted_Y.append(i)
        else:
            Adjusted_Y.append(i)
        #print(random_integer_array)
    return Adjusted_X, Adjusted_Y