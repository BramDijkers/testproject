#from permutation.MyVersion.getvalue import get_value
#
from MyVersion.ConfigManager import load_configurations
from MyVersion.getvalue import get_value
from MyVersion.ConfigManager import load_configurations
from MyVersion.filter import filter
import itertools
import pandas as pd
import os.path
from pathlib import Path
from os import path
import pickle


def my_func(params,count,configurations):
    for metric, range in params.items():
        print("hi")
    #mapper = {params[k]: configurations[k] for k in params.keys() & configurations.keys()}
def add_to_pickle(path, item):
    with open(path, 'ab') as file:
        pickle.dump(item, file, pickle.HIGHEST_PROTOCOL)
def read_from_pickle(path):
    with open(path, 'rb') as file:
        try:
            while True:
                yield pickle.load(file)
        except EOFError:
            pass
def main():
 configurations = load_configurations("permutation/MyVersion/config.json")
 doall = configurations["doall"]
 metrics = ['Clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
            "Stringy", "Monotonic"]
 if doall=="True":


    metrics=['Clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
                    "Stringy", "Monotonic"]
 if doall=="False":
    metrics = configurations["metrics"]
    metrics = (metrics.split(" "))
    #print(type(metrics))
    metrics2 = metrics
    #print(metrics2)
    ##print(len(metrics2))
    #print(metrics)
    #print(len(metrics))
 for i in metrics:
    if doall == "False":
         configurations = load_configurations("permutation/MyVersion/config.json")
         metrics = configurations["metrics"]
    else:
        metrics= i
    #print(f"this is met:{i}")
    count = 0
    testcount = 0
    configurations = load_configurations("permutation/MyVersion/config.json")
    #metrics = configurations["metrics"]

    savelocation = configurations["savelocation"]
    testfile = path.exists(f"permutation/data/{savelocation}/df/{metrics}.pkl")
    if testfile==True:
        os.remove(f"permutation/data/{savelocation}/df/{metrics}.pkl")
        print("we have delete a file, i am sorry")
    names=metrics.split()
    #print(names)
    #names = 'Striated Convex Skinny Stringy Outlying Clumpy Monotonic'.split()
    #   print(names)
    #Clumpy Outlying Skewed Sparse Striated Convex Skinny #Stringy Monotonic
    numberofnames=len(names)

    column_names = ['id', 'Clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
                    "Stringy", "Monotonic", 'image_location']
    print(len(names))

    faileddirections = []
    succesdirections= []
    subset=configurations["subset"]
    permutationsize=configurations["permutationsize"]
    repeatprocess = configurations["repeatprocess"]

    subsetsize = configurations["subsetsize"]
    maxIterations = configurations["maxIterations"]
    Strictness_of_points = configurations["Strictness_of_points"]
    Optimizer = configurations["Optimizer"]

    numberofclasses = configurations["Number_of_classes_per_metric"]
    sizeofclass = 1 / numberofclasses
    newclass = sizeofclass
    strictnessnew = newclass / 2
    maindf = pd.DataFrame(columns=column_names)
    maindf.to_pickle("permutation/data/combination/df/dataframe.pkl")
    classes= []
    howfar=0
    avg_itertaitions= []
    memory=0
    #numberofclasses = numberofclasses-1
    print(numberofclasses)
    sizeofclass=1/numberofclasses
    newclass=sizeofclass
    newclass= round(newclass, 3)
    strictnessnew= newclass/2
    print(strictnessnew)
    print(type(classes))
    mycount=0
    for i in range(numberofclasses):
        if mycount==0:
            classes.append(0)
            mycount= mycount+1
        classes.append(newclass)
        newclass=newclass+sizeofclass
        newclass= round(newclass, 5)
    #classes= [0.125, 0.375, 0.625, 0.875]
    print(classes)
    processnumber=0
    for i in range(repeatprocess):
        processnumber=processnumber+1
        for p in itertools.product(classes,repeat=numberofnames): #creates all possible combinations
            direction = dict(zip(names, p))  # the fit
            #breakloop = False
            breakloop = filter(direction)
            count = count + 1
            breakloop = False

            if breakloop == True:
                #print("Skipped")
                testcount=testcount+1



            #faileddirections, succesdirections = get_value(configurations, direction=direction, maindf=maindf, faileddirections=faileddirections, succesdirections=succesdirections)
            if breakloop==False:
                print(direction)
                time, status, direction, memory = get_value(configurations, direction=direction,memory=memory, processnumber=processnumber, metrics=metrics)
            #del AdjustedMeasures

                if status == "succes":
                    dictionary_copy = str(direction)
                    # succesdirections.append(dictionary_copy)
                    avg_itertaitions.append(time)
                    exist = path.exists(f"permutation/data/combination/DirectionList/{metrics}_succes.pkl")
                    PICKLE_FILE = (f"permutation/data/combination/DirectionList/{metrics}_succes.pkl")
                    if exist == False:
                        file_name = (f"permutation/data/combination/DirectionList/{metrics}_succes.pkl")
                        open_file = open(file_name, "wb")
                        list = []
                        list.append(dictionary_copy)
                        pickle.dump(list, open_file)
                        open_file.close()
                    if exist == True:
                        directionlist = pd.read_pickle(
                            f"permutation/data/combination/DirectionList/{metrics}_succes.pkl")
                        # dict.dictionary_copy()
                        series_obj = pd.Series(dictionary_copy)
                        directionlist.append(dictionary_copy)
                        with open(f"permutation/data/combination/DirectionList/{metrics}_succes.pkl", 'wb') as f:
                            pickle.dump(directionlist, f)
                if status == 'fail':
                    dictionary_copy = str(direction)
                    faileddirections.append(dictionary_copy)
                    exist = path.exists(f"permutation/data/combination/DirectionList/{metrics}_fail.pkl")
                    PICKLE_FILE = (f"permutation/data/combination/DirectionList/{metrics}_fail.pkl")
                    if exist == False:
                        file_name = (f"permutation/data/combination/DirectionList/{metrics}_fail.pkl")
                        open_file = open(file_name, "wb")
                        list = []
                        list.append(dictionary_copy)
                        pickle.dump(list, open_file)
                        open_file.close()
                    if exist == True:
                        directionlist = pd.read_pickle(f"permutation/data/combination/DirectionList/{metrics}_fail.pkl")
                        # dict.directionlist()
                        directionlist.append(dictionary_copy)
                        # directionlist = directionlist.append(series_obj, ignore_index=True)
                        with open(f"permutation/data/combination/DirectionList/{metrics}_succes.pkl", 'wb') as f:
                            pickle.dump(directionlist, f)

                #if memory >= 410000:
                #    print(avg_itertaitions)
                #    avg_value = sum(avg_itertaitions) / len(avg_itertaitions)
                #    print(avg_value)
                #       break
            print(count)
            print(memory)
            #print(succesdirections)
            if subset == "True":
                if count==subsetsize:
                    #maindf.to_pickle("permutation/data/df/dataframe.pkl")

                    print(avg_itertaitions)
                    avg_value = sum(avg_itertaitions) / len(avg_itertaitions)
                    print(avg_value)
                    break
    #print(f"Dataframes created for: {finalsucces}")
    avg_value = sum(avg_itertaitions) / len(avg_itertaitions)
    print(f"The succes scatterplot took on average {avg_value} tries")

    print(f"Total number of scatter plots tried {count}")
    print(f"Number of times failed: {len(faileddirections)}")
 #print(f"Dataframes could not be created for: {finalfailed}")
    succesdirections = count - testcount- len(faileddirections)
    print(f"Number of times succeeded: {succesdirections}")
    print(f"maxIterations: {maxIterations}")
    print(f"permutationsize: {permutationsize}")
    print(f"Optimizer: {Optimizer}")
    print(f"subsetsize: {subsetsize}")
    print(f"skipped number: {testcount}")


    #maindf.to_pickle("permutation/data/df/dataframe.pkl")


main()

