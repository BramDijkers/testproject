def isInFilterRange(silent, qmresult, direction, maxiterations, time, rangemargin):

 range = rangemargin
 #range = 0.001
 inrage=True

 for qmName, qmRange in qmresult.items():


    if qmName == "Outlying":
        if "Outlying" in direction:
            targetmin = direction["Outlying"] - range
            targetmax = direction["Outlying"] + range
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage
    if inrage == False:
        break

    if qmName == "Skewed":
        if "Skewed" in direction:
            targetmin = direction["Skewed"] - 1
            targetmax = direction["Skewed"] + 1
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage

    if inrage == False:
        break
    if qmName == "Clumpy":
        if "Clumpy" in direction:
            targetmin = direction["Clumpy"] - range
            targetmax = direction["Clumpy"] + range
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage

    if inrage == False:
        break

    if qmName == "Sparse":
        if "Sparse" in direction:
            targetmin = direction["Sparse"] - 1
            targetmax = direction["Sparse"] + 1
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage

    if inrage == False:
        break

    if qmName == "Striated":
        if "Striated" in direction:
            targetmin = direction["Striated"] - range
            targetmax = direction["Striated"] + range
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage

    if inrage == False:
        break
    if qmName == "Convex":
        if "Convex" in direction:
            targetmin = direction["Convex"] - range
            targetmax = direction["Convex"] + range
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage
    if inrage == False:
        break

    if qmName == "Skinny":
        if "Skinny" in direction:
            targetmin = direction["Skinny"] - range
            targetmax = direction["Skinny"] + range
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage
    if inrage == False:
        break
    if qmName == "Stringy":
        if "Stringy" in direction:
            targetmin = direction["Stringy"] - range
            targetmax = direction["Stringy"] + range
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage

    if inrage == False:
        break

    if qmName == "Monotonic":
        if "Monotonic" in direction:
            targetmin = direction["Monotonic"] - range
            targetmax = direction["Monotonic"] + range
            if targetmin <= qmRange and targetmax >= qmRange:
                inrage = True
            else:
                if time!=maxiterations:
                    inrage = False
                    return inrage
                else:
                    inrage = False
                    if silent != "True":
                        print(f"Failed because of the {qmName} metric. {qmRange} was not between {targetmin} and {targetmax}")
                    inrage = False
                    return inrage
    if inrage == False:
        break
 return inrage