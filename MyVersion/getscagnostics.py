from pyscagnostics import scagnostics
import os
import os
import rpy2.robjects as robjects
import rpy2.robjects as robjects
#from rpy2.robjects.packages import importr
#scagnostics = importr('scagnostics')
#path= 'permutation/MyVersion'

def getscagnostics(Xvalue,Yvalue, Scagnoticversion="R"):
    #print(Xvalue)
    #bram = True
    if Scagnoticversion=="Python" :
        measures, _ = scagnostics(Xvalue, Yvalue, remove_outliers=True)
        #for key in measures:
            # rounding to K using round()
        #    measures[key] = round(measures[key], 5)
        #print(type(measures))
        #print(f"measures in python {measures}")
    if Scagnoticversion == "R" :
        path = 'permutation/MyVersion/'
        measures = {}
        r_source = robjects.r['source']
        r_source(os.path.join(path, '../get_scag.r'))
        r_getname = robjects.globalenv['scags']
        scags = r_getname(robjects.FloatVector(Xvalue), robjects.FloatVector(Yvalue))
        measures['Outlying'] = scags[0]
        measures['Skewed'] = scags[1]
        measures['Clumpy'] = scags[2]
        measures['Sparse'] = scags[3]
        measures['Striated'] = scags[4]
        measures['Convex'] = scags[5]
        measures['Skinny'] = scags[6]
        measures['Stringy'] = scags[7]
        measures['Monotonic'] = scags[8]
        #for key in measures:
            # rounding to K using round()
        #    measures[key] = round(measures[key], 5)
        #print(f"measures in r {measures}")
        #print(measures)

    '''
    all_scags = {}
    r_source = robjects.r['source']
    r_source(os.path.join('permutation/MyVersion', 'get_scag.r'))
    r_getname = robjects.globalenv['scags']
    scags = r_getname(robjects.FloatVector(Xvalue), robjects.FloatVector(Yvalue))
    all_scags['outlying'] = scags[0]
    all_scags['skewed'] = scags[1]
    all_scags['clumpy'] = scags[2]
    all_scags['sparse'] = scags[3]
    all_scags['striated'] = scags[4]
    all_scags['convex'] = scags[5]
    all_scags['skinny'] = scags[6]
    all_scags['stringy'] = scags[7]
    all_scags['monotonic'] = scags[8]
    return all_scags
    '''
    return measures
