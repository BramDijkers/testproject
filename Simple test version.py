import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
import plotly.express as px
from random import randrange
import numpy as np
from os import path
import os.path


irand = randrange(50, 51)
from pyscagnostics import scagnostics

CurrentArray_X = np.random.randint (1, 100, irand)
CurrentArray_Y = np.random.randint(1, 100, irand)

column_names = ['Clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
                    "Stringy", "Monotonic", 'irand', 'image_location']
maindf = pd.DataFrame(columns=column_names)
my_file = Path(f"Basiclocation.pkl")
try:
     my_abs_path = my_file.resolve(strict=True)
except FileNotFoundError:
     maindf.to_pickle(f"Basiclocation.pkl")
testpath = path.isdir(f"images")
if testpath==False:
     os.mkdir(f"images")

measures, _ = scagnostics(CurrentArray_X, CurrentArray_Y)

Clumpy = measures.get("Clumpy")
Outlying = measures.get("Outlying")
Skewed = measures.get("Skewed")
Sparse = measures.get("Sparse")
Striated = measures.get("Striated")
Convex = measures.get("Convex")
Skinny = measures.get("Skinny")
Stringy = measures.get("Stringy")
Monotonic = measures.get("Monotonic")
print(measures)
df = pd.DataFrame({'x':CurrentArray_X, 'y':CurrentArray_Y})
fig = px.scatter(x=CurrentArray_X, y=CurrentArray_Y)
#fig.show()
#legend
fig.update_layout(showlegend=False)
fig.update_layout(paper_bgcolor='rgba(0,0,0,0)')
fig.update_layout(plot_bgcolor='rgba(0,0,0,0)')

#x axis
fig.update_xaxes(visible=False)

#y axis
fig.update_yaxes(visible=False)
#plt.axis('off')
#plt.show()
plt.scatter(CurrentArray_X, CurrentArray_Y)
plt.axis('off')
plt.savefig(f"secondimage.png")

images_dir = f"images"
image_name = f"Image2.png"
fig.write_image("Image2.png")
#fig.clf()
maindf = pd.read_pickle(f"Basiclocation.pkl")
series_obj = pd.Series(
    [Clumpy, Outlying, Skewed, Sparse, Striated, Convex, Skinny, Stringy, Monotonic, irand, image_name],
     index=maindf.columns)
maindf = maindf.append(series_obj, ignore_index=True)
#print(maindf.describe(include='all'))
maindf.to_pickle(f"Basiclocation.pkl")